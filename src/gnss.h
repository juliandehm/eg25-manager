/*
 * Copyright (C) 2021 Dylan Van Assche <me@dylanvanassche.be>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <time.h>
#include <curl/curl.h>

#include "manager.h"

void gnss_init(struct EG25Manager *manager, toml_table_t *config);
void gnss_destroy(struct EG25Manager *manager);
void gnss_upload_assistance_data(struct EG25Manager *manager);
